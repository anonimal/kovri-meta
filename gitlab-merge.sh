#!/usr/bin/env bash

# Copyright (c) 2015-2018, The Kovri Project
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are
# permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list
#    of conditions and the following disclaimer in the documentation and/or other
#    materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be
#    used to endorse or promote products derived from this software without specific
#    prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
# THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
# THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Parts of this file are originally copyright (c) 2015 fluffypony and The Monero Project

# This sloppy script will locally construct a merge commit for a pull request on
# a gitlab repository, inspect it, sign it, and push it.

# To use with github.com, look for the lines which say to uncomment for github,
# uncomment, and comment the gitlab-specific lines. Re-comment as necessary.

# Example: add the following lines to .git/config:
# [gitlabmerge]
# repository = kovri-project/kovri
# branch = master

export GPG_TTY=$(tty)

PULL="$1"
if [[ -z "$PULL" ]]; then
  echo "Usage: $0 <pull-request number>" >&2
  echo "Example: $0 123" >&2
  exit 2
fi

HOST="$(git config --get gitlabmerge.host)"
if [[ -z "$HOST" ]]; then
  # Upstream host (not *your* fork)
  HOST="git@kovri-project.gitlab.com:"
fi

BRANCH="$(git config --get gitlabmerge.branch)"
if [[ -z "$BRANCH" ]]; then
  BRANCH="master"
fi

TESTCMD="$(git config --get gitlabmerge.testcmd)"

REPO="$(git config --get gitlabmerge.repository)"
if [[ -z "$REPO" ]]; then
  echo "ERROR: No repository configured. Use this command to set:" >&2
  echo "git config gitlabmerge.repository <owner>/<repo>" >&2
  echo "In addition, you can set the following variables:" >&2
  echo "- gitlabmerge.host (default git@kovri-project.gitlab.com)" >&2
  echo "- gitlabmerge.branch (default master)" >&2
  echo "- gitlabmerge.testcmd (default none)" >&2
  exit 1
fi

proxy_git="/usr/bin/proxychains4 git"

# Initialize source branches
git checkout -q "$BRANCH"

#if $proxy_git fetch -v "${HOST}"/"${REPO}" "+refs/pull/${PULL}/*:refs/heads/pull/${PULL}/*"; then  # github
if $proxy_git fetch upstream "+refs/merge-requests/${PULL}/*:refs/heads/merge-requests/${PULL}/*"; then
  #if ! git log -q -1 "refs/heads/pull/${PULL}/head" >/dev/null 2>&1; then  # github
  if ! git log -q -1 "refs/heads/merge-requests/${PULL}/head" >/dev/null 2>&1; then
    echo "ERROR: Cannot find head of merge request #${PULL} on ${HOST}/${REPO}" >&2
    exit 3
  fi
  #if ! git log -q -1 "refs/heads/merge-requests/${PULL}/merge" >/dev/null 2>&1; then  # github
    #echo "ERROR: Cannot find merge of merge request #${PULL} on $HOST/$REPO" >&2
    #exit 3
  #fi
else
  echo "ERROR: Cannot find merge request #${PULL} on ${HOST}/${REPO}" >&2
  exit 3
fi

#if $proxy_git fetch -v "${HOST}"/"${REPO}" +refs/heads/"${BRANCH}":refs/heads/pull/"${PULL}"/base; then  # github
if $proxy_git fetch upstream +refs/heads/"${BRANCH}":refs/heads/merge-requests/"${PULL}"/base; then
  true
else
  echo "ERROR: Cannot find branch $BRANCH on ${HOST}/${REPO}." >&2
  exit 3
fi

# Uncomment for github
#git checkout -q pull/"${PULL}"/base
#git branch -q -D pull/"${PULL}"/local-merge 2>/dev/null
#git checkout -q -b pull/"${PULL}"/local-merge

git checkout -q merge-requests/"${PULL}"/base
git branch -q -D merge-requests/"${PULL}"/local-merge 2>/dev/null
git checkout -q -b merge-requests/"${PULL}"/local-merge
TMPDIR="$(mktemp -d -t ghmXXXXX)"

function cleanup()
{
  git checkout -q "$BRANCH"
# Uncomment for github
#  git branch -q -D pull/"${PULL}"/head 2>/dev/null
#  git branch -q -D pull/"${PULL}"/base 2>/dev/null
#  git branch -q -D pull/"${PULL}"/merge 2>/dev/null
#  git branch -q -D pull/"${PULL}"/local-merge 2>/dev/null
  git branch -q -D merge-requests/"${PULL}"/head 2>/dev/null
  git branch -q -D merge-requests/"${PULL}"/base 2>/dev/null
  git branch -q -D merge-requests/"${PULL}"/merge 2>/dev/null
  git branch -q -D merge-requests/"${PULL}"/local-merge 2>/dev/null
  rm -rf "$TMPDIR"
}

# Create unsigned merge commit.
(
  echo "Merge pull request #${PULL}"
  echo ""
  #git log --no-merges --topo-order --pretty='format:%h %s (%an)' pull/"${PULL}"/base..pull/"$PULL"/head  # github
  git log --no-merges --topo-order --pretty='format:%h %s (%an)' merge-requests/"${PULL}"/base..merge-requests/"${PULL}"/head
)>"${TMPDIR}/message"
#if git merge -q --commit --no-edit --no-ff -m "$(<"${TMPDIR}/message")" pull/"${PULL}"/head; then  # github
if git merge -q --commit --no-edit --no-ff -m "$(<"${TMPDIR}/message")" merge-requests/"${PULL}"/head; then
  if [ "d$(git log --pretty='format:%s' -n 1)" != "dMerge pull request #${PULL}" ]; then
    echo "ERROR: Creating merge failed (already merged?)." >&2
    cleanup
    exit 4
  fi
else
  echo "ERROR: Cannot be merged cleanly." >&2
  git merge --abort
  cleanup
  exit 4
fi

# Run test command if configured.
if [[ "$TESTCMD" ]]; then
  # Go up to the repository's root.
  while [ ! -d .git ]; do cd ..; done
  if ! $TESTCMD; then
    echo "ERROR: Running $TESTCMD failed." >&2
    cleanup
    exit 5
  fi
  # Show the created merge.
  # Uncomment for github
  #git diff pull/"$PULL"/merge..pull/"${PULL}"/local-merge >"${TMPDIR}"/diff
  #git diff pull/"$PULL"/base..pull/"${PULL}"/local-merge
  git diff merge_requests/"${PULL}"/merge..merge_requests/"${PULL}"/local-merge >"${TMPDIR}"/diff
  git diff merge_requests/"${PULL}"/base..merge_requests/"${PULL}"/local-merge
  if [[ "$(<"${TMPDIR}"/diff)" != "" ]]; then
    echo "WARNING: merge differs from gitlab!" >&2
    read -p "Type 'ignore' to continue. " -r >&2
    if [[ "d$REPLY" =~ ^d[iI][gG][nN][oO][rR][eE]$ ]]; then
      echo "Difference with gitlab ignored." >&2
    else
      cleanup
      exit 6
    fi
  fi
  read -p "Press 'd' to accept the diff. " -n 1 -r >&2
  echo
  if [[ "d$REPLY" =~ ^d[dD]$ ]]; then
    echo "Diff accepted." >&2
  else
    echo "ERROR: Diff rejected." >&2
    cleanup
    exit 6
  fi
# else
  # Verify the result.
  # echo "Dropping you on a shell so you can try building/testing the merged source." >&2
  # echo "Run 'git diff HEAD~' to show the changes being merged." >&2
  # echo "Type 'exit' when done." >&2
  # bash -i
  # read -p "Press 'm' to accept the merge. " -n 1 -r >&2
  # echo
  # if [[ "d$REPLY" =~ ^d[Mm]$ ]]; then
  #   echo "Merge accepted." >&2
  # else
  #   echo "ERROR: Merge rejected." >&2
  #   cleanup
  #   exit 7
  # fi
fi

# Sign the merge commit.
# read -p "Press 's' to sign off on the merge. " -n 1 -r >&2
# echo
# if [[ "d$REPLY" =~ ^d[Ss]$ ]]; then
  if [[ "$(git config --get user.signingkey)" == "" ]]; then
    echo "WARNING: No GPG signing key set, not signing. Set one using:" >&2
    echo "git config --global user.signingkey <key>" >&2
    git commit --signoff --amend --no-edit
  else
    git commit --gpg-sign --amend --no-edit
  fi
# fi
if [ $? != 0 ]; then
  echo "FATAL ERROR! Could not sign commit, aborting" >&2
  exit 1
fi

# Clean up temporary branches, and put the result in $BRANCH.
git checkout -q "$BRANCH"
#git reset -q --hard pull/"${PULL}"/local-merge  # github
git reset -q --hard merge-requests/"${PULL}"/local-merge
cleanup

# Push the result.
# read -p "Type 'push' to push the result to $HOST/$REPO, branch $BRANCH. " -r >&2
# if [[ "d$REPLY" =~ ^d[Pp][Uu][Ss][Hh]$ ]]; then
  # $proxy_git push "${HOST}"/"${REPO}" refs/heads/"${BRANCH}"
  echo -e 'Pushing to upstream...\nYou have 3 seconds to cancel...'
  $proxy_git push upstream refs/heads/"${BRANCH}"
  sleep 3s
  echo -e 'Fetching upstream...'
  $proxy_git fetch upstream
  echo -e 'Pushing to master...\nYou have 3 seconds to cancel...'
  sleep 3s
  $proxy_git push origin master
# fi
