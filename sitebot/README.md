# Sitebot
Jekyll test and build scripts for [#kovri-site](https://gitlab.com/kovri-project/kovri)

## Summary
Git provides a severely restricted login shell binary `git-shell` which restricts logged in users to push/pull functionality in an existing repository. This functionality can be extended arbitrarily by placing additional binaries or shell scripts in a `git-shell-commands` directory nested directly inside the `git-shell` user's home directory.

## Security
This system was designed to minimize the chances of someone accidently `rm -rf /`ing while still providing sufficient access to the site maintainers. **It is probably not escape proof and should not be assumed to be**. The primary security constraint is restricting access is via SSH authentication.

There has been very few (I believe 1) escape vulnerability discovered in the `git-shell` binary itself, which was promptly patched in 2014, but other common methods of bypassing restricted shells need to be accounted for, etc. Disabling extraneous SSH forwarding functionality and allowing the passage of arbitrary ENV variables being the most common.

The additional shell scripts included for our `git-shell` user take only preconfigured (or no) arguments, in order to minimize injection threats. However, this setup has not be subjected to intense scrutiny as it is designed to be run in a trusted scenario, so caveat emptor.

## Git-shell user
The "sitebot" user should be created as a normal (read no --system flag) user with a home directory that will house the source, test build, and final deployment of the [#kovri-site](https://gitlab.com/kovri-project/kovri-site) repository. We used the `/srv/site` folder as the "sitebot" user's home directory and the container for the test and site webroots. The test webroot is served from the test subdomain, and allows the site to be inspected and tested before deployment.

The only other requirement is that the user's login shell be set to `/usr/bin/git-shell` either when the user is created with the `useradd --shell /usr/bin/git-shell` flag, or by modifying and existing user in a similar fashion.


## Adding users
In a trusted scenario, by adding public keys to the `git-shell` user's `~/.ssh/authorized_keys` file will allow the owner of the corresponding private SSH key to log in as the `git-shell` user and execute the build scripts.

## Commands
The following commands are available and may be run interactively in the git shell or executed as command line parameters.

#### Basic commands:
  **`help`**   - show this help screen
  **`exit`**   - quits the shell (interactive mode only)

#### Test site commands:
  **`status`** - returns the `git status` of the local [#kovri-site](https://gitlab.com/kovri-project/kovri-site) repository
  **`pull`**   -  `git pull`s the lastest sources to the test [#kovri-site](https://gitlab.com/kovri-project/kovri-site)
  **`build`**  - `jekyll build`s the live test kovri-site from pulled sources

#### Production site commands:
  **`deploy`** - deploys the contents of the test kovri-site to production
  **`revert`** - reverts the production kovri-site to a previous state









